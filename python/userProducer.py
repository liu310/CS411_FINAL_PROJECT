import csv
import random
import string

'''

'''
if __name__ == '__main__':
	historyData=[] # [[name,[]],]
	restaurants=[]

	with open("C:\\Users\\Kathe\\Desktop\\CS411\\project\\dataScrawler\\yelpcrawl\\output.csv",'r') as restFile:
		reader=csv.reader(restFile)
		rowNum=0;
		for row in reader:
			if rowNum==0:
				header=row
			else:
				restaurants.append(row[0])
			rowNum+=1
	restFile.close()

	with open("C:\\Users\\Kathe\\Desktop\\CS411\\project\\fakedData\\history-basic.csv",'r') as file:
		reader=csv.reader(file)
		rowNum=0
		nRests=len(restaurants)
		idx=1
		for row in reader:
			if rowNum!=0:
				cur_name=row[0]
				cur_user=[]
				recNum=random.randint(30,80)
				# 20% of the users only go to 10 fixed restaurants
				if idx>=1 and idx <=20: 
					rest_n=random.sample(restaurants,18)
					rest_n_fixed=rest_n[0:9]
					rest_n_try=rest_n[10:17]
				#70% users go to 20 fixed resaurants and explore 10 more
				elif idx>20 and idx<=90: 
					rest_n=random.sample(restaurants,30)
					rest_n_fixed=rest_n[0:20]
					rest_n_try=rest_n[21:29]
				#10% users choose widely
				else: 
					rest_n=random.sample(restaurants,int(recNum*0.7))
					fixed_idx=int(len(rest_n)*0.2)
					rest_n_fixed=rest_n[0:fixed_idx]
					rest_n_try=rest_n[fixed_idx+1:len(rest_n)-1]
				for j in range(recNum):
					if j<0.8*recNum:
						cur_atti=random.randint(0,1)	
						rest_idx=random.randint(0,len(rest_n_fixed)-1)
						cur_rest=rest_n_fixed[rest_idx]
					else:
						cur_atti=random.randint(-1,1)
						rest_idx=random.randint(0,len(rest_n_try)-1)
						cur_rest=rest_n_try[rest_idx]
					cur_MM=str(random.randint(1,12))
					cur_DD=str(random.randint(1,30))
					cur_YY=str(random.randint(2015,2016))
					#cur_meal=str(random.randint(1,3))
					#cur_date=cur_MM+'/'+cur_DD+'/'+cur_YY+'/'+cur_meal
					cur_date=cur_MM+'/'+cur_DD+'/'+cur_YY
					cur_user.append([cur_name,cur_date,cur_rest,cur_atti])
				historyData.append(cur_user)
				idx+=1

			else:
				header=row

			rowNum+=1
	file.close()


	with open("hisory.csv",'w',newline='') as outfile:
		writer=csv.writer(outfile,delimiter=',')
		writer.writerow(['user','date','restaurant','attitude'])
		for user in historyData:
			for record in user:
				writer.writerow(record)
	outfile.close()

